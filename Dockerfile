FROM ubuntu:14.04

MAINTAINER E. Brian Welch <brian.welch@vanderbilt.edu>

# update ubuntu
RUN apt-get -y update

# install add-apt-repository
RUN apt-get install -y software-properties-common

# install LibreOffice 5.x without GNOME/KDE components
RUN add-apt-repository -y ppa:libreoffice/ppa
RUN apt-get -y update
RUN apt-get install -y libreoffice --no-install-recommends

# install unzip
RUN apt-get install -y unzip

# add pptx2txt.sh from public github.com release
ADD https://github.com/welcheb/pptx2txt.sh/archive/v0.1.0.tar.gz /root
RUN tar -xvf /root/v0.1.0.tar.gz -C /root/

# add slides2txt_internal.sh
ADD ./add_to_docker_image/slides2txt_internal.sh /root/

# array form of entrypoint to support additional argument from docker run command
ENTRYPOINT ["/bin/bash"]
