docker_slides2txt
=================

Dockerfile to build a docker container for converting PowerPoint or Keynote slides to a text file.

[![build status](https://gitlab.com/welcheb/docker_slides2txt/badges/master/build.svg)](https://gitlab.com/welcheb/docker_slides2txt/commits/master)

Overview
========

Docker images and containers (a running image) are similar to virtual machines except that the container operating system (OS) matches the host OS. That allows the container to be managed like a process with no boot-up required. This makes running an application in a container efficient and fast compared to using a virtual machine.

Currently docker containers run natively on **Linux**. On **Mac OS X** and **Windows**, a lightweight **Linux** virtual machine is required to run the docker daemon. Docker support for **Mac OS X** and **Windows** is available using the [Docker Toolbox](https://www.docker.com/products/docker-toolbox).

File input/output is handled using shared mounted volumes between the host and the container. Due to limitations of docker volume mounting when the host OS is not **Linux**, the installation of **docker_slides2txt** is recommended to be under the user's home directory. To process data, the user should change to the directory on the host OS to the directory holding the input data (which should also be under the user home directory) before invoking the `slides2txt_run.sh` script.

Getting Started
===============

1. Install [docker](https://www.docker.com).

  **Linux** installation from the command line
  ~~~
  wget -qO- https://get.docker.com | sh
  sudo usermod -aG docker <YOUR_USERNAME>
  reboot
  ~~~

  **Mac OS X** or **Windows**

  Download and install [Docker Toolbox](https://www.docker.com/products/docker-toolbox).

2. Open a command prompt terminal.

  **Linux** : open a normal terminal window

  **Mac OS X** or **Windows** : open **Docker Quickstart Terminal** application

3. Download this git repository, e.g., to your home directory.

  ~~~
  git clone https://gitlab.com/welcheb/docker_slides2txt.git ~/docker_slides2txt
  ~~~

4. Add the path to `slides2txt_run.sh` to the `PATH` environment variable. Below it is assumed you cloned the git repository into your home directory.

  ~~~
  echo 'export PATH=$PATH:~/docker_slides2txt' >> ~/.profile
  source ~/.profile
  ~~~

5. Use the `slides2txt_run.sh` script to convert slides to text. The first time the script is run will cause the `registry.gitlab.com/welcheb/docker_slides2txt:latest` image to be downloaded. The download is approximately several hundred megabytes and may take some time to download depending on your internet connection speed.

  Convert provided sample slides to text
  ~~~
  cd ~/docker_slides2txt/test_slides/
  slides2txt_run.sh shakespeare_pptx.pptx
  slides2txt_run.sh shakespeare_ppt.ppt
  slides2txt_run.sh shakespeare_key.key
  ~~~

Known Issues
------------
* Keynote 09 is not correctly converted by LibreOffice
